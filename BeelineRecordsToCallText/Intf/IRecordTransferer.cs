﻿using System.Threading.Tasks;

namespace BeelineRecordsToCallText.Intf
{
    /// <summary>
    /// Перенос записей из личного кабинета Билайн в CallText
    /// </summary>
    public interface IRecordTransferer
    {
        /// <summary>
        /// Доступность следующего цикла переноса
        /// </summary>
        /// <returns></returns>
        bool CanTransfer();
        
        /// <summary>
        /// Произвести перенос 1 пакета
        /// </summary>
        /// <returns>Количестве перенесенных записей</returns>
        Task<int> TransferNextPacketAsync();
    }
}