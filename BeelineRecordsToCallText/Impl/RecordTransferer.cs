﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using BeelineAPItoFTP.Configuration;
using BeelineRecordsToCallText.Eceptions;
using BeelineRecordsToCallText.Intf;
using BeelineRecordsToCallText.Models;

namespace BeelineRecordsToCallText.Impl
{
    /// <summary>
    /// Загрузка файлов из личного кабинет Билайн в CallText
    /// </summary>
    public class RecordTransferer : IRecordTransferer
    {
        private readonly ApplicationSettings _settings;
        private readonly HttpClient _beelineHttpClient;
        private readonly HttpClient _callTextHttpClient;

        private int _transferedCounter;
        private bool _isRecordsFound;
        
        private int _innerCounter;
        
        public RecordTransferer(
            ApplicationSettings settings,
            HttpClient beelineHttpClient,
            HttpClient callTextHttpClient)
        {
            _settings = settings;
            _beelineHttpClient = beelineHttpClient;
            _callTextHttpClient = callTextHttpClient;
            _transferedCounter = 0;
            _isRecordsFound = true;
        }

        public bool CanTransfer()
        {
            return _transferedCounter < _settings.TransferSettings.RecordTransferLimit && _isRecordsFound;
        }

        public async Task<int> TransferNextPacketAsync()
        {
            try
            {
                return await TransferNextPacketInnerAsync();
            }
            catch (RecordTransferException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new RecordTransferException(ex.Message, _innerCounter);
            }
        }

        private async Task<int> TransferNextPacketInnerAsync()
        {
            _innerCounter = 0;
            
            UriBuilder uriBuilder = new(_settings.BeelineApiSettings.RecordsUrl)
            {
                Query = $"id={_settings.TransferSettings.LastUploadedRecord}"
            };
            
            var response = await _beelineHttpClient.GetAsync(uriBuilder.ToString());
            if (!response.IsSuccessStatusCode)
            {
                _isRecordsFound = false;
                return _innerCounter;
            }

            var responseMsg = await response.Content.ReadAsStringAsync();

            if (string.IsNullOrEmpty(responseMsg))
            {
                _isRecordsFound = false;
                return _innerCounter;
            }
            
            var recordDataArr = JsonSerializer.Deserialize<Record[]>(responseMsg, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                PropertyNameCaseInsensitive = true
            });

            foreach (var record in recordDataArr)
            {
                if (_transferedCounter >= _settings.TransferSettings.RecordTransferLimit)
                {
                    return _innerCounter;
                }

                var category =
                    _settings.CallTextApiSettings.Categories.FirstOrDefault(
                        x => x.Extension == record.Abonent.Extension);

                if (category == null)
                {
                    Console.WriteLine(
                        $"Не задана категория для записи [recordId={record.Id}, вн.номер={record.Abonent.Extension}]. Запись пропущена.");
                    continue;
                }
                
                var recordStream =
                    await _beelineHttpClient.GetStreamAsync(_settings.BeelineApiSettings.GetFileDownloadUrl(record.Id));
                var formData = new MultipartFormDataContent();
                formData.Add(new StreamContent(recordStream), "file", $"{record.Id}.mp3");
                formData.Add(new StringContent(category.Code.ToString()), "category");

                var paramsString = record.Date.ToString("dd_MM_yyyy_hh_mm_ss") + "_" + record.Abonent.Extension;
                formData.Add(new StringContent(paramsString), "params");

                _beelineHttpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Token", _settings.CallTextApiSettings.AccessToken);
                var filePostResult = await _callTextHttpClient.PostAsync(_settings.CallTextApiSettings.UploadFileUrl, formData);

                if (filePostResult.IsSuccessStatusCode)
                {
                    _transferedCounter++;
                    UpdateLastUploadedId(record.Id);
                    _innerCounter++;
                }
                else
                {
                    throw new RecordTransferException(
                        $"Не удалось загрузить запись разговора в сервис CallText.ru: {await filePostResult.Content.ReadAsStringAsync()}",
                        _innerCounter);
                }
            }

            return _innerCounter;
        }

        /// <summary>
        /// Обновить конфиг mainsettings.json
        /// </summary>
        /// <param name="lastUploadedId">Значение свойства LastRecordId</param>
        private void UpdateLastUploadedId(string lastUploadedId)
        {
            //todo: идея сторить логику на постоянном обновлении конфигурационного файла кажется неудачной, может стоит
            //писать в БД? например, sqlite
            var configPath = Path.Combine(Directory.GetCurrentDirectory(), Program.MainSettingsConfigName);
            _settings.TransferSettings.LastUploadedRecord = lastUploadedId;
            var json = JsonSerializer.Serialize(_settings, new JsonSerializerOptions
            {
                WriteIndented = true,

            });

            using var writer = new StreamWriter(configPath);
            writer.Write(json);
        }
    }
}