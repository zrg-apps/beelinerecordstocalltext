﻿using System;
using System.Text.Json.Serialization;

namespace BeelineRecordsToCallText.Models
{
    public class Record
    {
        public string Id { get; set; }
        public string ExternalId { get; set; }
        public string Phone { get; set; }
        public string Direction { get; set; }
        [JsonConverter(typeof(MicrosecondToDateTimeConverter))]
        public DateTime Date { get; set; }
        public int Duration { get; set; }
        public int FileSize { get; set; }
        public Abonent Abonent { get; set; }
    }
}