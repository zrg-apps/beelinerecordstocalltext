﻿namespace BeelineRecordsToCallText.Models
{
    public class Abonent
    {
        public string UserId { get; set; }
        public string Phone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Department { get; set; }
        public string Extension { get; set; }
    }
}