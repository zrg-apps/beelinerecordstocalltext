﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BeelineRecordsToCallText.Models
{
    public class MicrosecondToDateTimeConverter : JsonConverter<DateTime>
    {
        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var dateStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);
            return dateStart.AddMilliseconds(reader.GetInt64());
        }

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }
    }
}