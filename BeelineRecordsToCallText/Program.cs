﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BeelineAPItoFTP.Configuration;
using BeelineRecordsToCallText.Eceptions;
using BeelineRecordsToCallText.Impl;
using Microsoft.Extensions.Configuration;

namespace BeelineRecordsToCallText
{
    internal static class Program
    {
        public const string MainSettingsConfigName = "mainsettings.json";
        public const string AppSettingsConfigName = "appsettings.json";
        
        private static IConfigurationRoot _configuration;

        static async Task Main(string[] args)
        {
            Console.WriteLine("Получение конфигурации.");
            
            SetConfiguration();
            
            var settigns = GetApplicationSettings();
            using var beelineHttpClient = GetBeelineHttpClient(settigns);
            using var callTextHttpClient = GetCallTextHttpClient(settigns);
            
            var transferedRecordsCount = 0;
            var fileTranserer = new RecordTransferer(settigns, beelineHttpClient, callTextHttpClient);
            
            var errorsCount = 5;
            
            while (fileTranserer.CanTransfer())
            {
                Console.WriteLine("Запуск цикла переноса записей.");

                try
                {
                    var currentTransferedCount= await fileTranserer.TransferNextPacketAsync();
                    transferedRecordsCount += currentTransferedCount;
                    Console.WriteLine($"В текущем цикле перенесено {currentTransferedCount}.");
                    errorsCount = 5;
                }
                catch (RecordTransferException ex)
                {
                    Console.WriteLine(
                        --errorsCount > 0
                            ? $"Ошибка во время переноса записей: {ex.Message}. Перенесено в текущем цикле записей: " +
                              $"{ex.SuccessBeforeCount}. Осталось попыток повторного запуска: {errorsCount}"
                            : $"Произошла 5 ошибок подряд, выполнение программы завершено: {ex.Message}");
                }
            }
            
            Console.WriteLine($"Всего перенесено в сервис calltext.ru: {transferedRecordsCount}");
            Console.ReadLine();
        }

        private static void SetConfiguration()
        {
            IConfigurationBuilder builder = new ConfigurationBuilder();
            builder.AddJsonFile(AppSettingsConfigName, false);
            builder.AddJsonFile(MainSettingsConfigName, optional: false, reloadOnChange: true);
            _configuration = builder.Build();
        }

        private static ApplicationSettings GetApplicationSettings()
        {
            ApplicationSettings settigns = new();
            _configuration.Bind(settigns);

            if (settigns.TransferSettings.RecordTransferLimit == 0)
            {
                settigns.TransferSettings.RecordTransferLimit = int.MaxValue;
            }
            if (string.IsNullOrEmpty(settigns.TransferSettings.LastUploadedRecord))
            {
                settigns.TransferSettings.LastUploadedRecord = "-1";
            }

            if (settigns.CallTextApiSettings.Categories == null ||
                settigns.CallTextApiSettings.Categories.Any(x => x == null))
            {
                throw new Exception("Ошибка конфигурации: не заданы или не правильно заданы категории.");
            }

            return settigns;
        }

        private static HttpClient GetBeelineHttpClient(ApplicationSettings settings)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("X-MPBX-API-AUTH-TOKEN", settings.BeelineApiSettings.AccessToken);
            return client;
        }
        
        private static HttpClient GetCallTextHttpClient(ApplicationSettings settings)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Token", settings.CallTextApiSettings.AccessToken);
            return client;
        }
    }
}