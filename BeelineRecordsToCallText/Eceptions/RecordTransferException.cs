﻿using System;

namespace BeelineRecordsToCallText.Eceptions
{
    public class RecordTransferException : Exception
    {
        /// <summary>
        /// Количество успешных отправок до возникновения исключения
        /// </summary>
        public int SuccessBeforeCount;
        public override string Message { get; }

        public RecordTransferException(string message, int successBeforeCount)
        {
            SuccessBeforeCount = successBeforeCount;
            Message = message;
        }
    }
}