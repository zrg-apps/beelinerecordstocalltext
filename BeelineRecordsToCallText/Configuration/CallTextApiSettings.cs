﻿namespace BeelineAPItoFTP.Configuration
{
    /// <summary>
    /// Настройки calltext.ru
    /// </summary>
    public class CallTextApiSettings
    {
        /// <summary>
        /// URL API для загрузки файла в calltext.ru 
        /// </summary>
        public string UploadFileUrl { get; set; }
        
        /// <summary>
        /// Токен авторизации
        /// </summary>
        public string AccessToken { get; set; }
        
        /// <summary>
        /// Категории/группы в calltext.ru
        /// </summary>
        public Category[] Categories { get; set; }
    }
}