﻿namespace BeelineAPItoFTP.Configuration
{
    /// <summary>
    /// Настройки доступа к api Билайн
    /// </summary>
    public class BeelineApiSettings
    {
        /// <summary>
        /// URL для открытия записей
        /// </summary>
        public string RecordsUrl { get; set; }
        
        /// <summary>
        /// URL для скачивания mp3 записей. Для конструирования готового URL необходимо
        /// вызвать метод <see cref="GetFileDownloadUrl"/>
        /// </summary>
        public string DownloadFileUrl { get; set; }
        
        /// <summary>
        /// Токен доступа в личный кабинет
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// Получить ссылку для скачивания файла
        /// </summary>
        /// <param name="recordId">Идентификатор записи</param>
        /// <returns></returns>
        public string GetFileDownloadUrl(string recordId)
        {
            return $"{DownloadFileUrl}/{recordId}/download";
        }
    }
}