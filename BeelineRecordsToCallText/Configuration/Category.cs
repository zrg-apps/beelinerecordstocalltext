﻿using System;

namespace BeelineAPItoFTP.Configuration
{
    /// <summary>
    /// Категория
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Название категории
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Код категории в calltext.ru
        /// </summary>
        public Guid Code { get; set; }
        
        /// <summary>
        /// Вн. номер оператора
        /// </summary>
        public string Extension { get; set; }
    }
}