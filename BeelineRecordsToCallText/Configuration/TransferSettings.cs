﻿using Microsoft.Extensions.Configuration;

namespace BeelineAPItoFTP.Configuration
{
    /// <summary>
    /// Общие настройки переноса записей разговоров
    /// </summary>
    public class TransferSettings
    {
        /// <summary>
        /// Идентификатор последнего загруженного файла в calltext.ru. При загрузке списка записей из
        /// личного кабинета Билайн, загружается следующая запись после текущего. При первом запуске
        /// приложение значение должно быть -1.
        /// </summary>
        public string LastUploadedRecord { get; set; }
        
        /// <summary>
        /// Масксимальное количество записей, которые нужно обработать за цикл запуска
        /// </summary>
        public int? RecordTransferLimit { get; set; }
    }
}