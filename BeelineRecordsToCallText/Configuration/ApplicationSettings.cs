﻿namespace BeelineAPItoFTP.Configuration
{
    /// <summary>
    /// Настройки уровня бизнес-логики
    /// </summary>
    public class ApplicationSettings
    {
        /// <summary>
        /// Настройки доступа к личному кабинету Билайн
        /// </summary>
        public BeelineApiSettings BeelineApiSettings { get; set; }
        
        /// <summary>
        /// Настройки доступа к API calltext.ru
        /// </summary>
        public CallTextApiSettings CallTextApiSettings { get; set; }
        
        /// <summary>
        /// Настройки перенеса записей в calltext.ru
        /// </summary>
        public TransferSettings TransferSettings { get; set; }
    }
}